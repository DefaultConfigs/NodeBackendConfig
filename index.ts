
import configDev from './webpack/webpack.dev';
import configProd from './webpack/webpack.prod';

export {configDev, configProd};
