import config from './webpack.common';
import * as webpack from 'webpack';
import * as merge from 'webpack-merge';
const NpmInstallPlugin = require('npm-install-webpack-plugin');

const configProd = merge(
    config,
    {
        plugins: [
            new webpack.EnvironmentPlugin({
                NODE_ENV: 'production',
                DEBUG: false
            }),

            new webpack.optimize.UglifyJsPlugin({
                mangle: false,
                comments: false,
                sourceMap: false,
            })
        ]
    });

export default configProd;
