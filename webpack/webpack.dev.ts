import config from './webpack.common';
import * as webpack from 'webpack';
import * as merge from 'webpack-merge';
const NpmInstallPlugin = require('npm-install-webpack-plugin');

const configDev = merge(
    config,
    {
        plugins: [
            new webpack.EnvironmentPlugin({
                NODE_ENV: 'development',
                DEBUG: true
            })
        ]
    });

export default configDev;
