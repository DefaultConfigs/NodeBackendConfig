import configProd from './webpack.prod';
import * as webpack from 'webpack';
import * as path from 'path';
import * as merge from 'webpack-merge';

const config = merge(
    configProd,
    {
        entry: {
            'server': path.join(__dirname, "../test/main.ts"),
        },
        output: {
            path: path.resolve(__dirname, '../dist'),
            filename: '[name].js',
        },
    }
)

export default config;
