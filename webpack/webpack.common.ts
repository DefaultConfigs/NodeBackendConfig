import * as webpack from 'webpack';
import * as nodeexternals from 'webpack-node-externals';


const config: webpack.Configuration = {
    target: 'node',
    externals: [nodeexternals()],

    node: {
        __dirname: false,
        fs: 'empty'
    },

    devtool: false,

    resolve: {
        extensions: ['.ts', '.js']
    },

    module : {

        rules : [
            {
                test: /\.ts$/,
                use: {
                    loader: 'awesome-typescript-loader'
                }
            },
            
            {
                test: /\.node$/,
                use: 'node-loader'
            }
        ]
    },

    plugins : []
};

export default config;
